/*
 * Copyright (c) 2019 Andreas Baierl <ichgeh@imkreisrum.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef H_LIMA_CMD_STREAM_DUMP
#define H_LIMA_CMD_STREAM_DUMP

#define MALI_ADDRESSES 0x40

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

struct mali_address {
   void *address; /* mapped address */
   unsigned int size;
   unsigned int physical; /* actual address */
} mali_addresses[MALI_ADDRESSES];

static const char *PIPE_COMPARE_FUNC_STRING[] = {
   "NEVER",    /* 0 */
   "LESS",     /* 1 */
   "EQUAL",    /* 2 */
   "LEQUAL",   /* 3 */
   "GREATER",  /* 4 */
   "NOTEQUAL", /* 5 */
   "GEQUAL",   /* 6 */
   "ALWAYS",   /* 7 */
};

static const char *PIPE_STENCIL_OP_STRING[] = {
   "KEEP",      /* 0 */
   "REPLACE",   /* 1 */
   "ZERO",      /* 2 */
   "INVERT",    /* 3 */
   "INCR_WRAP", /* 4 */
   "DECR_WRAP", /* 5 */
   "INCR",      /* 6 */
   "DECR",      /* 7 */
};

static const char *PIPE_BLEND_FUNC_STRING[] = {
   "SUBTRACT",     /* 0 */
   "REV_SUBTRACT", /* 1 */
   "ADD",          /* 2 */
   "",             /* 3 */
   "BLEND_MIN",    /* 4 */
   "BLEND_MAX",    /* 5 */
};

static const char *PIPE_BLENDFACTOR_STRING[] = {
   "SRC_COLOR",        /* 0 */
   "DST_COLOR",        /* 1 */
   "CONST_COLOR",      /* 2 */
   "ZERO",             /* 3 */
   "", "", "",         /* 4 - 6 */
   "SRC_ALPHA_SAT",    /* 7 */
   "INV_SRC_COLOR",    /* 8 */
   "INV_DST_COLOR",    /* 9 */
   "INV_CONST_COLOR",  /* 10 */
   "ONE",              /* 11 */
   "", "", "", "",     /* 12 - 15 */
   "SRC_ALPHA",        /* 16 */
   "DST_ALPHA",        /* 17 */
   "CONST_ALPHA",      /* 18 */
   "", "", "", "", "", /* 19 - 23 */
   "INV_SRC_ALPHA",    /* 24 */
   "INV_DST_ALPHA",    /* 25 */
   "INV_CONST_ALPHA",  /* 26 */

};

static inline const char
*get_compare_func_string(int func) {
   if ((func >= 0) && (func <= 7))
      return PIPE_COMPARE_FUNC_STRING[func];
   else
      return "";
}

static inline const char
*get_stencil_op_string(int func) {
   if ((func >= 0) && (func <= 7))
      return PIPE_STENCIL_OP_STRING[func];
   else
      return "";
}

static inline const char
*get_blend_func_string(int func) {
   if ((func >= 0) && (func <= 5))
      return PIPE_BLEND_FUNC_STRING[func];
   else
      return "";
}

static inline const char
*get_blendfactor_string(int func) {
   if ((func >= 0) && (func <= 26))
      return PIPE_BLENDFACTOR_STRING[func];
   else
      return "";
}

typedef struct __attribute__((__packed__)) {
   /* Word 0 */
   uint32_t format : 6;
   uint32_t flag1: 1;
   uint32_t swap_r_b: 1;
   uint32_t unknown_0_1: 8;
   uint32_t stride: 15;
   uint32_t unknown_0_2: 1;

   /* Word 1-3 */
   uint32_t unknown_1_1: 7;
   uint32_t unnorm_coords: 1;
   uint32_t unknown_1_2: 1;
   uint32_t texture_type: 3;
   uint32_t min_lod: 8; /* Fixed point, 4.4, unsigned */
   uint32_t max_lod: 8; /* Fixed point, 4.4, unsigned */
   uint32_t lod_bias: 9; /* Fixed point, signed, 1.4.4 */
   uint32_t unknown_2_1: 3;
   uint32_t has_stride: 1;
   uint32_t min_mipfilter_2: 2; /* 0x3 for linear, 0x0 for nearest */
   uint32_t min_img_filter_nearest: 1;
   uint32_t mag_img_filter_nearest: 1;
   uint32_t wrap_s_clamp_to_edge: 1;
   uint32_t wrap_s_clamp: 1;
   uint32_t wrap_s_mirror_repeat: 1;
   uint32_t wrap_t_clamp_to_edge: 1;
   uint32_t wrap_t_clamp: 1;
   uint32_t wrap_t_mirror_repeat: 1;
   uint32_t unknown_2_2: 3;
   uint32_t width: 13;
   uint32_t height: 13;
   uint32_t unknown_3_1: 1;
   uint32_t unknown_3_2: 15;

   /* Word 4 */
   uint32_t unknown_4;

   /* Word 5 */
   uint32_t unknown_5;

   /* Word 6-15 */
   /* layout is in va[0] bit 13-14 */
   /* VAs start in va[0] at bit 30, each VA is 26 bits (only MSBs are stored), stored
    * linearly in memory */
   union {
      uint32_t va[0];
      struct __attribute__((__packed__)) {
         uint32_t unknown_6_1: 13;
         uint32_t layout: 2;
         uint32_t unknown_6_2: 9;
         uint32_t unknown_6_3: 6;
#define VA_BIT_OFFSET 30
#define VA_BIT_SIZE 26
         uint32_t va_0: VA_BIT_SIZE;
         uint32_t va_0_1: 8;
         uint32_t va_1_x[0];
      } va_s;
   };
} lima_tex_desc;

int find_mem_block(uint32_t address, uint32_t *offset, uint32_t *end);
int wrap_log(const char *format, ...);
void wrap_log_bits(int value, int nr_bits);
uint32_t parse_plbu(uint32_t desc[], int stop, int start, int vs_end, unsigned physical, int continued);
uint32_t parse_vs(uint32_t desc[], int stop, int start, int vs_end, unsigned physical, int continued);

#endif
