/*
 * Copyright (c) 2019 Andreas Baierl <ichgeh@imkreisrum.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <stddef.h>
#include <stdint.h>

#include "lima_cmd_stream_dump.h"
#include "lima_util.h"

static void
parse_attributes_descriptor(uint32_t desc[], uint32_t start,  uint32_t num)
{
   int i;
   uint32_t *value1, *value2;
   for (i = 0; i < num * 2; i += 2) {
      value1 = &desc[start + i];
      value2 = &desc[start + i + 1];
      wrap_log("\t\tattribute %d @ 0x%08x, desc: 0x%08x\n", (i / 2) + 1, *value1, *value2);
   }
}

static void
parse_varyings_descriptor(uint32_t desc[], uint32_t start,  uint32_t num)
{
   int i;
   uint32_t *value1, *value2;
   for (i = 0; i < num * 2; i += 2) {
      value1 = &desc[start + i];
      value2 = &desc[start + i + 1];
      wrap_log("\t\tvarying %d @ 0x%08x, desc: 0x%08x\n", (i / 2) + 1, *value1, *value2);
   }
}

static void
parse_vary_attr_desc(uint32_t attr_addr, uint32_t num, int is_attr)
{
   int array_idx;
   uint32_t offset;

   array_idx = find_mem_block(attr_addr, &offset, NULL);
   if (!array_idx)
      return;

   if (is_attr)
      parse_attributes_descriptor(mali_addresses[array_idx - 1].address, offset / 4, num);
   else
      parse_varyings_descriptor(mali_addresses[array_idx - 1].address, offset / 4, num);
}

static void
parse_uniform(uint32_t desc[], uint32_t start,  uint32_t size)
{
   int i;
   uint32_t *value1, *value2, *value3, *value4;
   for (i = 0; i < (size / 4); i += 4) {
      value1 = &desc[start + i];
      value2 = &desc[start + i + 1 ];
      value3 = &desc[start + i + 2 ];
      value4 = &desc[start + i + 3 ];
      wrap_log("\t\tuniform %d: 0x%08x 0x%08x 0x%08x 0x%08x (%lf, %lf, %f, %f)\n", i / 4,
                *value1, *value2, *value3, *value4,
                *(float *)value1, *(float *)value2, *(float *)value3, *(float *)value4);
   }
}

static void
find_uniforms(uint32_t addr, uint32_t size)
{
   int array_idx;
   uint32_t offset;

   array_idx = find_mem_block(addr, &offset, NULL);
   if (!array_idx)
      return;

   parse_uniform(mali_addresses[array_idx - 1].address, offset / 4, size);
}

static void
parse_vs_draw(uint32_t *value1, uint32_t *value2)
{
   if ((*value1 == 0x00000000) && (*value2 == 0x00000000))
      wrap_log("\t/* ---EMPTY CMD */\n");
   else
      wrap_log("\t/* DRAW: num: %d, index_draw: %s */\n",
               (*value1 & 0xff000000) >> 24 | (*value2 & 0x000000ff) << 8,
               (*value1 & 0x00000001) ? "true" : "false");
}

static void
parse_vs_shader_info(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* SHADER_INFO: prefetch: %d, size: %d */\n",
            (*value1 & 0xfff00000) >> 20,
            (((*value1 & 0x000fffff) >> 10) + 1) << 4);
}

static void
parse_vs_unknown1(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* UNKNOWN_1 */\n");
}

static void
parse_vs_varying_attribute_count(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* VARYING_ATTRIBUTE_COUNT: nr_vary: %d, nr_attr: %d */\n",
            ((*value1 & 0x00ffffff) >> 8) + 1, (*value1 >> 24) + 1);
}

static void
parse_vs_attributes_address(uint32_t *value1, uint32_t *value2)
{
   uint32_t num = (*value2 & 0x0fffffff) >> 17;

   wrap_log("\t/* ATTRIBUTES_ADDRESS: address: 0x%08x, size: %d */\n",
            *value1, num);
   parse_vary_attr_desc(*value1, num, 1);
}

static void
parse_vs_varyings_address(uint32_t *value1, uint32_t *value2)
{
   uint32_t num = (*value2 & 0x0fffffff) >> 17;

   wrap_log("\t/* VARYINGS_ADDRESS: varying info @ 0x%08x, size: %d */\n",
            *value1, num);
   parse_vary_attr_desc(*value1, num, 0);
}

static void
parse_vs_uniforms_address(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* UNIFORMS_ADDRESS (GP): address: 0x%08x, size: %d */\n",
            *value1, (*value2 & 0x0fffffff) >> 12);
   if (*value1)
      find_uniforms(*value1, (*value2 & 0x0fffffff) >> 12);
}

static void
parse_vs_shader_address(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* SHADER_ADDRESS (GP): address: 0x%08x, size: %d */\n",
            *value1, (*value2 & 0x0fffffff) >> 12);
}

static void
parse_vs_semaphore(uint32_t *value1, uint32_t *value2)
{
   if (*value1 == 0x00028000)
      wrap_log("\t/* SEMAPHORE_BEGIN_1 */\n");
   else if (*value1 == 0x00000001)
      wrap_log("\t/* SEMAPHORE_BEGIN_2 */\n");
   else if (*value1 == 0x00000000)
      wrap_log("\t/* SEMAPHORE_END: index_draw disabled */\n");
   else if (*value1 == 0x00018000)
      wrap_log("\t/* SEMAPHORE_END: index_draw enabled */\n");
   else
      wrap_log("\t/* SEMAPHORE - cmd unknown! */\n");
}

static void
parse_vs_unknown2(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* UNKNOWN_2 */\n");
}

static void
parse_vs_continue(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* CONTINUE: at 0x%08x */\n", *value1);
}

uint32_t
parse_vs(uint32_t desc[],
         int stop,
         int start,
         int vs_end,
         unsigned physical,
         int continued)
{
   uint32_t *value1;
   uint32_t *value2;
   uint32_t ret = 0;
   int end;
   int i;

   if (vs_end < start)
      end = stop;
   else
      end = MIN(vs_end, stop);

   if (!continued)
      wrap_log("/* ============ VS CMD STREAM BEGIN ============= */\n");
   else
      wrap_log("/* ============ VS CMD STREAM CONTINUED ========= */\n");

   for (i = start; i < end; i += 2) {
      value1 = &desc[i];
      value2 = &desc[i + 1];
      wrap_log("/* 0x%08x (0x%08x) */\t0x%08x 0x%08x",
               (i * 4) + physical, (i - start) * 4, *value1, *value2);

      if ((*value2 & 0xffff0000) == 0x00000000)
         parse_vs_draw(value1, value2);
      else if ((*value2 & 0xff0000ff) == 0x10000040)
         parse_vs_shader_info(value1, value2);
      else if ((*value2 & 0xff0000ff) == 0x10000041)
         parse_vs_unknown1(value1, value2);
      else if ((*value2 & 0xff0000ff) == 0x10000042)
         parse_vs_varying_attribute_count(value1, value2);
      else if ((*value2 & 0xff0000ff) == 0x20000000)
         parse_vs_attributes_address(value1, value2);
      else if ((*value2 & 0xff0000ff) == 0x20000008)
         parse_vs_varyings_address(value1, value2);
      else if ((*value2 & 0xff000000) == 0x30000000)
         parse_vs_uniforms_address(value1, value2);
      else if ((*value2 & 0xff000000) == 0x40000000) {
         parse_vs_shader_address(value1, value2);
         if (value1) {
            lima_disasm(*value1, 0, (*value2 & 0x0fffffff) >> 12);
         }
      }
      else if ((*value2  & 0xff000000)== 0x50000000)
         parse_vs_semaphore(value1, value2);
      else if ((*value2 & 0xff000000) == 0x60000000)
         parse_vs_unknown2(value1, value2);
      else if ((*value2 & 0xff000000) == 0xf0000000) {
         parse_vs_continue(value1, value2);
         return *value1;
      }
      else
         wrap_log("\t/* --- unknown cmd --- */\n");
   }
   wrap_log("/* ============ VS CMD STREAM END =============== */\n");
   wrap_log("\n");

   return ret;
}
