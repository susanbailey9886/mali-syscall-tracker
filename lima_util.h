/*
 * Copyright (c) 2019 Andreas Baierl <ichgeh@imkreisrum.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef H_LIMA_UTIL
#define H_LIMA_UTIL

#include <stdbool.h>
#include <stdint.h>

static inline float
mali_half_to_float(uint16_t f16) {
   union fi {
      float f;
      int32_t i;
      uint32_t ui;
   };

   union fi infnan;
   union fi magic;
   union fi f32;

   infnan.ui = 0x8f << 23;
   infnan.f = 65536.0f;
   magic.ui = 0xef << 23;

   f32.ui = (f16 & 0x7fff) << 13;
   f32.f *= magic.f;

   if (f32.f >= infnan.f)
      f32.ui |= 0xff << 23;

   f32.ui |= (uint32_t)(f16 & 0x8000) << 16;

   return f32.f;
}

void lima_disasm(uint32_t prog, bool is_frag, uint32_t size);

#endif
