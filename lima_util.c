/*
 * Copyright (c) 2020 Andreas Baierl <ichgeh@imkreisrum.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 *
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "lima_cmd_stream_dump.h"
#include "lima_gp_codegen.h"
#include "lima_pp_codegen.h"

void
lima_disasm(uint32_t addr, bool is_frag, uint32_t size)
{
   int n;

   int array_idx;
   uint32_t offset;

   array_idx = find_mem_block(addr, &offset, NULL);
   if (!array_idx)
      return;

   uint32_t *value;
   value = &((uint32_t *)(mali_addresses[array_idx - 1].address))[offset / 4];

   if (is_frag) {
      uint32_t *bin = value;
      uint32_t offt = 0;

      uint32_t next_instr_length = 0;
      do {
         ppir_codegen_ctrl *ctrl = (ppir_codegen_ctrl *)bin;
         wrap_log("\t@%6d: ", offt);
         ppir_disassemble_instr(bin, offt);
         bin += ctrl->count;
         offt += ctrl->count;
         next_instr_length = ctrl->next_count;
      } while (next_instr_length);
   }
   else {
      gpir_disassemble_program((gpir_codegen_instr *)value, size / (sizeof(gpir_codegen_instr)));
   }
}
