#!makefile

MALI_API_VERSION ?= r4p0

ifeq ($(MALI_API_VERSION),r4p0)
	CONFIG = -DCONFIG_MALI_API_R4P0
else ifeq ($(MALI_API_VERSION),r6p1)
	CONFIG = -DCONFIG_MALI_API_R6P1
else ifeq ($(MALI_API_VERSION),r6p2)
	CONFIG = -DCONFIG_MALI_API_R6P2
else ifeq ($(MALI_API_VERSION),r7p0)
	CONFIG = -DCONFIG_MALI_API_R7P0
endif

libMali_wrap.so: main.c lima_plbu_cmd_stream_dump.c lima_vs_cmd_stream_dump.c lima_util.c lima_gp_disasm.c lima_pp_disasm.c
	gcc -shared -std=gnu99 -fPIC -g $(CONFIG) -o $@ $^ -ldl -lpthread
