/*
 * Copyright (c) 2019 Andreas Baierl <ichgeh@imkreisrum.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "lima_cmd_stream_dump.h"
#include "lima_util.h"

static void find_uniform_info(uint32_t tex_addr, uint32_t count);

typedef struct {
   char *info;
} render_state_info;

static render_state_info render_state_infos[] = {
   { .info = "BLEND_COLOR_BG", },
   { .info = "BLEND_COLOR_RA", },
   { .info = "ALPHA_BLEND", },
   { .info = "DEPTH_TEST", },
   { .info = "DEPTH_RANGE", },
   { .info = "STENCIL_FRONT", },
   { .info = "STENCIL_BACK", },
   { .info = "STENCIL_TEST", },
   { .info = "MULTI_SAMPLE", },
   { .info = "SHADER_ADDRESS", },
   { .info = "VARYING_TYPES", },
   { .info = "UNIFORMS_ADDRESS", },
   { .info = "TEXTURES_ADDRESS", },
   { .info = "AUX0", },
   { .info = "AUX1", },
   { .info = "VARYINGS_ADDRESS", },
};

/* borrowed from mesa's util/u_math.h */
static inline float
ubyte_to_float(uint8_t ub)
{
   return (float) ub * (1.0f / 255.0f);
}

static inline float
ushort_to_float(uint16_t us)
{
   return (float) us * (1.0f / 65535.0f);
}

static inline float
lima_fixed8_to_float(int16_t i)
{
   float sign = 1.0;

   if (i > 0xff) {
      i = 0x200 - i;
      sign = -1;
   }

   return sign * (float)(i / 16.0);
}

static void
parse_plbu_block_step(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* BLOCK_STEP: shift_min: %d, shift_h: %d, shift_w: %d */\n",
            (*value1 & 0xf0000000) >> 28,
            (*value1 & 0x0fff0000) >> 16,
            *value1 & 0x0000ffff);
}

static void
parse_plbu_tiled_dimensions(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* TILED_DIMENSIONS: tiled_w: %d, tiled_h: %d */\n",
            ((*value1 & 0xff000000) >> 24) + 1,
            ((*value1 & 0x00ffff00) >> 8) + 1);
}

static void
parse_plbu_block_stride(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* BLOCK_STRIDE: block_w: %d */\n", *value1 & 0x000000ff);
}

static void
parse_plbu_array_address(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* ARRAY_ADDRESS: gp_stream: 0x%08x, block_num (block_w * block_h): %d */\n",
            *value1, (*value2 & 0x00ffffff) + 1);
}

static void
parse_plbu_viewport_left(float *value1, uint32_t *value2)
{
   wrap_log("\t/* VIEWPORT_LEFT: viewport_left: %f */\n", *value1);
}

static void
parse_plbu_viewport_right(float *value1, uint32_t *value2)
{
   wrap_log("\t/* VIEWPORT_RIGHT: viewport_right: %f */\n", *value1);
}

static void
parse_plbu_viewport_bottom(float *value1, uint32_t *value2)
{
   wrap_log("\t/* VIEWPORT_BOTTOM: viewport_bottom: %f */\n", *value1);
}

static void
parse_plbu_viewport_top(float *value1, uint32_t *value2)
{
   wrap_log("\t/* VIEWPORT_TOP: viewport_top: %f */\n", *value1);
}

static void
parse_plbu_semaphore(uint32_t *value1, uint32_t *value2)
{
   if (*value1 == 0x00010002)
      wrap_log("\t/* ARRAYS_SEMAPHORE_BEGIN */\n");
   else if (*value1 == 0x00010001)
      wrap_log("\t/* ARRAYS_SEMAPHORE_END */\n");
   else
      wrap_log("\t/* SEMAPHORE - cmd unknown! */\n");
}

static void
parse_plbu_primitive_setup(uint32_t *value1, uint32_t *value2)
{
   if (*value1 == 0x00000200)
      wrap_log("\t/* UNKNOWN_2 (PRIMITIVE_SETUP INIT?) */\n");
   else
      wrap_log("\t/* PRIMITIVE_SETUP: %scull: %d (0x%x), index_size: %d */\n",
               (*value1 & 0x1000) ? "force point size, " : "",
               (*value1 & 0x000f0000) >> 16, (*value1 & 0x000f0000) >> 16,
               (*value1 & 0x00000e00) >> 9);
}

static uint32_t
parse_plbu_rsw_vertex_array(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* RSW_VERTEX_ARRAY: rsw: 0x%08x, gl_pos: 0x%08x */\n",
            *value1,
            (*value2 & 0x0fffffff) << 4);
   return *value1;
}

static void
parse_plbu_scissors(uint32_t *value1, uint32_t *value2)
{
   float minx = (*value1 & 0xc0000000) >> 30 | (*value2 & 0x00001fff) << 2;
   float maxx = ((*value2 & 0x0fffe000) >> 13) + 1;
   float miny = *value1 & 0x00003fff;
   float maxy = ((*value1 & 0x3fff8000) >> 15) + 1;

   wrap_log("\t/* SCISSORS: minx: %f, maxx: %f, miny: %f, maxy: %f */\n",
            minx, maxx, miny, maxy);
}

static void
parse_plbu_unknown_1(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* UNKNOWN_1 */\n");
}

static void
parse_plbu_low_prim_size(float *value1, uint32_t *value2)
{
   wrap_log("\t/* LOW_PRIM_SIZE: size: %f */\n", *value1);
}

static void
parse_plbu_depth_range_near(float *value1, uint32_t *value2)
{
   wrap_log("\t/* DEPTH_RANG_NEAR: depth_range: %f */\n", *value1);
}

static void
parse_plbu_depth_range_far(float *value1, uint32_t *value2)
{
   wrap_log("\t/* DEPTH_RANGE_FAR: depth_range: %f */\n", *value1);
}

static void
parse_plbu_indexed_dest(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* INDEXED_DEST: gl_pos: 0x%08x */\n", *value1);
}

static void
parse_plbu_indexed_pt_size(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* INDEXED_PT_SIZE: pt_size: 0x%08x */\n", *value1);
}

static void
parse_plbu_indices(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* INDICES: indices: 0x%08x */\n", *value1);
}

static void
parse_plbu_draw_arrays(uint32_t *value1, uint32_t *value2)
{
   if ((*value1 == 0x00000000) && (*value2 == 0x00000000)) {
      wrap_log("\t/* ---EMPTY CMD */\n");
      return;
   }

   uint32_t count = (*value1 & 0xff000000) >> 24 | (*value2 & 0x000000ff) << 8;
   uint32_t start = *value1 & 0x00ffffff;
   uint32_t mode = (*value2 & 0x001f0000) >> 16;

   wrap_log("\t/* DRAW_ARRAYS: count: %d, start: %d, mode: %d (0x%x) */\n",
            count, start, mode, mode);
}

static void
parse_plbu_draw_elements(uint32_t *value1, uint32_t *value2)
{
   uint32_t count = (*value1 & 0xff000000) >> 24 | (*value2 & 0x000000ff) << 8;
   uint32_t start = *value1 & 0x00ffffff;
   uint32_t mode = (*value2 & 0x001f0000) >> 16;

   wrap_log("\t/* DRAW_ELEMENTS: count: %d, start: %d, mode: %d (0x%x) */\n",
            count, start, mode, mode);
}

static void
parse_plbu_continue(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* CONTINUE: continue at 0x%08x */\n", *value1);
}

static void
parse_plbu_end(uint32_t *value1, uint32_t *value2)
{
   wrap_log("\t/* END (FINISH/FLUSH) */\n");
}

/* parse rsw and return TEXTURE_ADDRESS if available */
static uint32_t
parse_rsw(uint32_t desc[], unsigned physical, uint32_t start, int i)
{
   uint32_t *value = &desc[start + i];
   uint32_t *helper = NULL;

   wrap_log("\t0x%08x\t/* %s", *value, render_state_infos[i].info);

   switch (i) {
   case 0: /* BLEND COLOR BG */
      wrap_log(": blend_color.color[1] = %f, blend_color.color[2] = %f */\n",
               (float)(ubyte_to_float((*value & 0xffff0000) >> 16)),
               (float)(ubyte_to_float(*value & 0x0000ffff)));
      break;
   case 1: /* BLEND COLOR RA */
      wrap_log(": blend_color.color[3] = %f, blend_color.color[0] = %f */\n",
               (float)(ubyte_to_float((*value & 0xffff0000) >> 16)),
               (float)(ubyte_to_float(*value & 0x0000ffff)));
      break;
   case 2: /* ALPHA BLEND */
      wrap_log("(1): colormask 0x%02x, rgb_func %d (%s), alpha_func %d (%s) */\n",
               (*value & 0xf0000000) >> 28, /* colormask */
               (*value & 0x00000007),
               get_blend_func_string(*value & 0x00000007), /* rgb_func */
               (*value & 0x00000038) >> 3,
               get_blend_func_string((*value & 0x00000038) >> 3)); /* alpha_func */
      /* add a few tabs for alignment */
      wrap_log("\t\t\t\t\t\t/* %s(2)", render_state_infos[i].info);
      wrap_log(": rgb_src_factor %d (%s), rbg_dst_factor %d (%s) */\n",
               (*value & 0x000007c0) >> 6,
               get_blendfactor_string((*value & 0x000007c0) >> 6), /* rgb_src_factor */
               (*value & 0x0000f800) >> 11,
               get_blendfactor_string((*value & 0x0000f800) >> 11)); /* rgb_dst_factor */
      /* add a few tabs for alignment */
      wrap_log("\t\t\t\t\t\t/* %s(3)", render_state_infos[i].info);
      wrap_log(": alpha_src_factor %d (%s), alpha_dst_factor %d (%s), bits 24-27 0x%02x (",
               (*value & 0x000f0000) >> 16,
               get_blendfactor_string((*value & 0x000f0000) >> 16), /* alpha_src_factor */
               (*value & 0x00f00000) >> 20,
               get_blendfactor_string((*value & 0x00f00000) >> 20), /* alpha_dst_factor */
               (*value & 0x0f000000) >> 24);
      wrap_log_bits((*value & 0x0f000000) >> 24, 4); /* bits 24-27 */
      wrap_log(") */\n");
      break;
   case 3: /* DEPTH TEST */
      if ((*value & 0x00000001) == 0x00000001)
         wrap_log("(1): depth test enabled && writes allowed */\n");
      else
         wrap_log("(1): depth test disabled || writes not allowed */\n");

      wrap_log("\t\t\t\t\t\t/* %s(2)", render_state_infos[i].info);
      wrap_log(": depth_func %d (%s)", (*value & 0x0000000e) >> 1,
               get_compare_func_string((*value & 0x0000000e) >> 1));
      wrap_log(", offset_scale: %d", (*value & 0x00ff0000) >> 16);
      wrap_log(", offset_units: %d", (*value & 0xff000000) >> 24);
      if (*value & 0x400)
         wrap_log(", shader writes depth or stencil");
      if (*value & 0x800)
         wrap_log(", shader writes depth");
      if (*value & 0x1000)
         wrap_log(", shader writes stencil");
      wrap_log(" */\n\t\t\t\t\t\t/* %s(3)", render_state_infos[i].info);
      wrap_log(": unknown bits 4-9: 0x%08x (", *value & 0x000003f0);
      wrap_log_bits((*value & 0x000003f0) >> 4, 6);
      wrap_log("), unknown bits 13-15: 0x%08x (", *value & 0x0000e000);
      wrap_log_bits((*value & 0x0000e000) >> 13, 3);
      wrap_log(") */\n");
      break;
   case 4: /* DEPTH RANGE */
      wrap_log(": viewport.far = %f, viewport.near = %f */\n",
               (float)(ushort_to_float((*value & 0xffff0000) >> 16)),
               (float)(ushort_to_float(*value & 0x0000ffff)));
      break;
   case 5: /* STENCIL FRONT */
      wrap_log("(1): valuemask 0x%02x, ref value %d (0x%02x), stencil_func %d (%s)*/\n",
               (*value & 0xff000000) >> 24, /* valuemask */
               (*value & 0x00ff0000) >> 16, (*value & 0x00ff0000) >> 16, /* ref value */
               (*value & 0x00000007),
               get_compare_func_string(*value & 0x00000007)); /* stencil_func */
      wrap_log("\t\t\t\t\t\t/* %s(2)", render_state_infos[i].info);
      wrap_log(": fail_op %d (%s), zfail_op %d (%s), zpass_op %d (%s), unknown (12-15) 0x%02x (",
               (*value & 0x00000038) >> 3,
               get_stencil_op_string((*value & 0x00000038) >> 3), /* fail_op */
               (*value & 0x000001c0) >> 6,
               get_stencil_op_string((*value & 0x000001c0) >> 6), /* zfail_op */
               (*value & 0x00000e00) >> 9,
               get_stencil_op_string((*value & 0x00000e00) >> 9), /* zpass_op */
               (*value & 0x0000f000) >> 12);
      wrap_log_bits((*value & 0x0000f000) >> 12, 4); /* unknown */
      wrap_log(") */\n");
      break;
   case 6: /* STENCIL BACK */
      wrap_log("(1): valuemask 0x%02x, ref value %d (0x%02x), stencil_func %d (%s)*/\n",
               (*value & 0xff000000) >> 24, /* valuemask */
               (*value & 0x00ff0000) >> 16, (*value & 0x00ff0000) >> 16, /* ref value */
               (*value & 0x00000007),
               get_compare_func_string(*value & 0x00000007)); /* stencil_func */
      wrap_log("\t\t\t\t\t\t/* %s(2)", render_state_infos[i].info);
      wrap_log(": fail_op %d (%s), zfail_op %d (%s), zpass_op %d (%s), unknown (12-15) 0x%02x (",
               (*value & 0x00000038) >> 3,
               get_stencil_op_string((*value & 0x00000038) >> 3), /* fail_op */
               (*value & 0x000001c0) >> 6,
               get_stencil_op_string((*value & 0x000001c0) >> 6), /* zfail_op */
               (*value & 0x00000e00) >> 9,
               get_stencil_op_string((*value & 0x00000e00) >> 9), /* zpass_op */
               (*value & 0x0000f000) >> 12);
      wrap_log_bits((*value & 0x0000f000) >> 12, 4); /* unknown */
      wrap_log(") */\n");
      break;
   case 7: /* STENCIL TEST */
      wrap_log("(1): stencil_front writemask 0x%02x, stencil_back writemask 0x%02x */\n",
               (*value & 0x000000ff), /* front writemask */
               (*value & 0x0000ff00) >> 8); /* back writemask */
      wrap_log("\t\t\t\t\t\t/* %s(2)", render_state_infos[i].info);
      wrap_log(": unknown (bits 16-31) 0x%04x (",
               (*value & 0xffff0000) >> 16);
      wrap_log_bits((*value & 0xffff0000) >> 16, 16); /* unknown, alpha ref_value? */
      wrap_log(") */\n");
      break;
   case 8: /* MULTI SAMPLE */
      if ((*value & 0x00000f00) == 0x00000000)
         wrap_log(": points");
      else if ((*value & 0x00000f00) == 0x00000400)
         wrap_log(": lines");
      else if ((*value & 0x00000f00) == 0x00000800)
         wrap_log(": triangles");
      else
         wrap_log(": unknown");

      if ((*value & 0x00000078) == 0x00000068)
         wrap_log(", fb_samples */\n");
      else if ((*value & 0x00000078) == 0x00000000)
         wrap_log(" */\n");
      else
         wrap_log(", UNKNOWN\n");
      break;
   case 9: /* SHADER ADDRESS */
      wrap_log(" (FS): fs shader @ 0x%08x, first instr length: %d */\n",
               *value & 0xffffffe0, *value & 0x0000001f);
      if (*value)
         lima_disasm(*value & 0xffffffe0, 1, *value & 0x1f);
      break;
   case 10: /* VARYING TYPES */
      helper = &desc[start + 15];
      wrap_log("(1): ");
      int val, j;
      /* 0 - 5 */
      for (j = 0; j < 6; j++) {
         val = *value & (0x07 << (j * 3));
         val = val >> (j * 3);
         wrap_log("val %d-%d, ", j, val);
      }
      /* 6 - 9 */
      /* add a few tabs for alignment */
      wrap_log("\n\t\t\t\t\t\t/* %s(2): ", render_state_infos[i].info);
      for (j = 6; j < 10; j++) {
         val = *value & (0x07 << (j * 3));
         val = val >> (j * 3);
         wrap_log("val %d-%d, ", j, val);
      }
      /* 10 */
      val = ((*value & 0xc0000000) >> 30) | ((*helper & 0x00000001) << 2);
      wrap_log("val %d-%d, ", j, val);
      j++;
      /* 11 */
      val = (*helper & 0x0000000e) >> 1;
      wrap_log("val %d-%d */\n", j, val);
      break;
   case 11: /* UNIFORMS ADDRESS */
      wrap_log(" (PP): pp uniform info @ 0x%08x, bits: 0x%01x */\n",
               *value & 0xfffffff0, *value & 0x0000000f);
      if (*value)
         find_uniform_info(*value & 0xfffffff0, *value & 0x0000000f);
      break;
   case 12: /* TEXTURES ADDRESS */
      wrap_log(": address: 0x%08x */\n", *value);
      if (*value)
         return *value;
      break;
   case 13: /* AUX0 */
      wrap_log("(1): varying_stride: %d", /* bits 0 - 4 varying stride, 8 aligned */
               (*value & 0x0000001f) << 3);
      if ((*value & 0x00000020) == 0x00000020) /* bit 5 has num_samplers */
         wrap_log(", num_samplers %d",
                  (*value & 0xffffc000) >> 14); /* bits 14 - 31 num_samplers */

      if ((*value & 0x00000080) == 0x00000080) /* bit 7 has_fs_uniforms */
         wrap_log(", has_fs_uniforms */\n");
      else
         wrap_log(" */\n");

      wrap_log("\t\t\t\t\t\t/* %s(2):", render_state_infos[i].info);
      if ((*value & 0x00000200) == 0x00000200) /* bit 9 early-z */
         wrap_log(" early-z enabled");
      else
         wrap_log(" early-z disabled");

      if ((*value & 0x00001000) == 0x00001000) /* bit 12 pixel-kill */
         wrap_log(", pixel kill enabled");
      else
         wrap_log(", pixel kill disabled");

      if ((*value & 0x00000040) == 0x00000040) /* bit 6 unknown */
         wrap_log(", bit 6 set");

      if ((*value & 0x00000100) == 0x00000100) /* bit 8 unknown */
         wrap_log(", bit 8 set");

      if (((*value & 0x00000c00) >> 10) > 0 ) /* bit 10 - 11 unknown */
         wrap_log(", bit 10 - 11: %d", ((*value & 0x00000c00) >> 10));

      if ((*value & 0x00002000) == 0x00002000) /* bit 13 unknown */
         wrap_log(", bit 13 set");
      wrap_log(" */\n");
      break;
   case 14: /* AUX1 */
      wrap_log(": ");
      if ((*value & 0x00002000) == 0x00002000)
         wrap_log("blend->base.dither true, ");
      if ((*value & 0x00010000) == 0x00010000)
         wrap_log("ctx->const_buffer[PIPE_SHADER_FRAGMENT].buffer true ");
      wrap_log("*/\n");
      break;
   case 15: /* VARYINGS ADDRESS */
      wrap_log(": varyings @ 0x%08x */\n", *value & 0xfffffff0);
      break;
   default: /* should never be executed! */
      wrap_log(": something went wrong!!! */\n");
      break;
   }

   return 0;
}

static void
parse_tex_desc(uint32_t *data, uint32_t start, uint32_t physical)
{
   uint32_t i = 0;
   lima_tex_desc *desc = (lima_tex_desc *)&data[start];

   /* Word 0 */
   wrap_log("/* 0x%08x (0x%08x) */\t0x%08x\n",
            physical + ((start + i) * 4), i * 4,
            *(&data[i + start]));
   i++;
   wrap_log("\t format: 0x%x (%d)\n", desc->format, desc->format);
   wrap_log("\t flag1: 0x%x (%d)\n", desc->flag1, desc->flag1);
   wrap_log("\t swap_r_b: 0x%x (%d)\n", desc->swap_r_b, desc->swap_r_b);
   wrap_log("\t unknown_0_1: 0x%x (%d)\n", desc->unknown_0_1, desc->unknown_0_1);
   wrap_log("\t stride: 0x%x (%d)\n", desc->stride, desc->stride);
   wrap_log("\t unknown_0_2: 0x%x (%d)\n", desc->unknown_0_2, desc->unknown_0_2);

   /* Word 1 - 3 */
   wrap_log("/* 0x%08x (0x%08x) */\t0x%08x 0x%08x 0x%08x\n",
            physical + ((start + i) * 4), i * 4,
            *(&data[i + start]),
            *(&data[i + 1 + start]),
            *(&data[i + 2 + start]));
   i += 3;
   wrap_log("\t unknown_1_1: 0x%x (%d)\n", desc->unknown_1_1, desc->unknown_1_1);
   wrap_log("\t unnorm_coords: 0x%x (%d)\n", desc->unnorm_coords, desc->unnorm_coords);
   wrap_log("\t unknown_1_2: 0x%x (%d)\n", desc->unknown_1_2, desc->unknown_1_2);
   wrap_log("\t texture_type: 0x%x (%d)\n", desc->texture_type, desc->texture_type);
   wrap_log("\t min_lod: 0x%x (%d) (%f)\n", desc->min_lod, desc->min_lod, lima_fixed8_to_float(desc->min_lod));
   wrap_log("\t max_lod: 0x%x (%d) (%f)\n", desc->max_lod, desc->max_lod, lima_fixed8_to_float(desc->max_lod));
   wrap_log("\t lod_bias: 0x%x (%d) (%f)\n", desc->lod_bias, desc->lod_bias, lima_fixed8_to_float(desc->lod_bias));
   wrap_log("\t unknown_2_1: 0x%x (%d)\n", desc->unknown_2_1, desc->unknown_2_1);
   wrap_log("\t has_stride: 0x%x (%d)\n", desc->has_stride, desc->has_stride);
   wrap_log("\t min_mipfilter_2: 0x%x (%d)\n", desc->min_mipfilter_2, desc->min_mipfilter_2);
   wrap_log("\t min_img_filter_nearest: 0x%x (%d)\n", desc->min_img_filter_nearest, desc->min_img_filter_nearest);
   wrap_log("\t mag_img_filter_nearest: 0x%x (%d)\n", desc->mag_img_filter_nearest, desc->mag_img_filter_nearest);
   wrap_log("\t wrap_s_clamp_to_edge: 0x%x (%d)\n", desc->wrap_s_clamp_to_edge, desc->wrap_s_clamp_to_edge);
   wrap_log("\t wrap_s_clamp: 0x%x (%d)\n", desc->wrap_s_clamp, desc->wrap_s_clamp);
   wrap_log("\t wrap_s_mirror_repeat: 0x%x (%d)\n", desc->wrap_s_mirror_repeat, desc->wrap_s_mirror_repeat);
   wrap_log("\t wrap_t_clamp_to_edge: 0x%x (%d)\n", desc->wrap_t_clamp_to_edge, desc->wrap_t_clamp_to_edge);
   wrap_log("\t wrap_t_clamp: 0x%x (%d)\n", desc->wrap_t_clamp, desc->wrap_t_clamp);
   wrap_log("\t wrap_t_mirror_repeat: 0x%x (%d)\n", desc->wrap_t_mirror_repeat, desc->wrap_t_mirror_repeat);
   wrap_log("\t unknown_2_2: 0x%x (%d)\n", desc->unknown_2_2, desc->unknown_2_2);
   wrap_log("\t width: 0x%x (%d)\n", desc->width, desc->width);
   wrap_log("\t height: 0x%x (%d)\n", desc->height, desc->height);
   wrap_log("\t unknown_3_1: 0x%x (%d)\n", desc->unknown_3_1, desc->unknown_3_1);
   wrap_log("\t unknown_3_2: 0x%x (%d)\n", desc->unknown_3_2, desc->unknown_3_2);

   /* Word 4 */
   wrap_log("/* 0x%08x (0x%08x) */\t0x%08x\n",
            physical + ((start + i) * 4), i * 4,
            *(&data[i + start]));
   i++;
   wrap_log("\t unknown_4: 0x%x (%d)\n", desc->unknown_4, desc->unknown_4);

   /* Word 5 */
   wrap_log("/* 0x%08x (0x%08x) */\t0x%08x\n",
            physical + ((start + i) * 4), i * 4,
            *(&data[i + start]));
   i++;
   wrap_log("\t unknown_5: 0x%x (%d)\n", desc->unknown_5, desc->unknown_5);

   /* Word 6 - */
   wrap_log("/* 0x%08x (0x%08x) */",
            physical + ((start + i) * 4), i * 4);
   wrap_log("\t");

   int miplevels = (int)lima_fixed8_to_float(desc->max_lod);
   for (int k = 0; k < ((((miplevels + 1) * 26) + 64) / 32); k++)
      wrap_log("0x%08x ", *(&data[i + start + k]));
   wrap_log("\n");

   i++;
   wrap_log("\t unknown_6_1: 0x%x (%d)\n", desc->va_s.unknown_6_1, desc->va_s.unknown_6_1);
   wrap_log("\t layout: 0x%x (%d)\n", desc->va_s.layout, desc->va_s.layout);
   wrap_log("\t unknown_6_2: 0x%x (%d)\n", desc->va_s.unknown_6_2, desc->va_s.unknown_6_2);
   wrap_log("\t unknown_6_3: 0x%x (%d)\n", desc->va_s.unknown_6_3, desc->va_s.unknown_6_3);

   /* first level */
   wrap_log("\t va_0: 0x%x \n", desc->va_s.va_0 << 6);

   /* second level up to desc->miplevels */
   int j;
   unsigned va_bit_idx;
   unsigned va_idx;
   uint32_t va;
   uint32_t va_1;
   uint32_t va_2;
   for (j = 1; j <= miplevels; j++) {
      va = 0;
      va_1 = 0;
      va_2 = 0;

      va_bit_idx = VA_BIT_OFFSET + (VA_BIT_SIZE * j);
      va_idx = va_bit_idx / 32;
      va_bit_idx %= 32;

      /* the first (32 - va_bit_idx) bits */
      va_1 |= (*(&data[i + start + va_idx - 1]) >> va_bit_idx);

      /* do we need some bits from the following word? */
      if (va_bit_idx > 6) {
         /* shift left and right again to erase the unneeded bits, keep space for va1 */
         va_2 |= (*(&data[i + start + va_idx]) << (2 * 32 - VA_BIT_SIZE - va_bit_idx));
         va_2 >>= ((2 * 32 - VA_BIT_SIZE - va_bit_idx) - (32 - va_bit_idx));
         va |= va_2;
      }
      va |= va_1;
      va <<= 6;
      wrap_log("\t va_%d: 0x%x \n", j, va);
   }
}

static void
find_texture_descriptor(uint32_t desc[], uint32_t start)
{
   int array_idx;
   uint32_t offset;
   uint32_t *value = &desc[start];
   array_idx = find_mem_block(*value, &offset, NULL);

   wrap_log("/* ============ TEXTURE DESC AT 0x%08x ======== */\n", *value);
   parse_tex_desc(mali_addresses[array_idx - 1].address,
                  offset / 4,
                  mali_addresses[array_idx - 1].physical);
   wrap_log("/* ============ TEXTURE DESC END ============== */\n");
   wrap_log("\n");
}

static void
find_textures(uint32_t tex_addr)
{
   int array_idx;
   uint32_t offset;
   array_idx = find_mem_block(tex_addr, &offset, NULL);

   wrap_log("/* ============ TEXTURES AT 0x%08x ============ */\n", tex_addr);
   find_texture_descriptor(mali_addresses[array_idx - 1].address, offset / 4);
}

static void
parse_uniforms(uint32_t *data, uint32_t start, uint32_t physical, uint32_t count)
{
   int i;
   uint32_t *value1;
   uint32_t *value2;

   for (i = 0; i < (1 << count) * 2; i += 2) {
      value1 = (uint32_t *)&data[start + i];
      value2 = (uint32_t *)&data[start + i + 1];
      wrap_log("\t\tuniform %d @ 0x%08x (0x%08x)\t 0x%08x 0x%08x (%f, %f, %f, %f)\n",
               i / 2, physical + ((start + i) * 4), i * 4,
               *value1,
               *value2,
               mali_half_to_float(*value1 >> 16),
               mali_half_to_float(*value1 & 0xffff),
               mali_half_to_float(*value2  >> 16),
               mali_half_to_float(*value2 & 0xffff));
   }
}


static void
find_uniform(uint32_t desc[], uint32_t start, uint32_t count)
{
   int array_idx;
   uint32_t offset;
   uint32_t *value = &desc[start];
   array_idx = find_mem_block(*value, &offset, NULL);

   wrap_log("%d uniforms @ 0x%08x ======== */\n", (1 << count), *value);
   parse_uniforms(mali_addresses[array_idx - 1].address,
                  offset / 4,
                  mali_addresses[array_idx - 1].physical, count);
}

static void
find_uniform_info(uint32_t addr, uint32_t count)
{
   int array_idx;

   uint32_t offset;
   array_idx = find_mem_block(addr, &offset, NULL);

   wrap_log("\t\t/* ====== uniforms list @ 0x%08x, ", addr);
   find_uniform(mali_addresses[array_idx - 1].address, offset / 4, count);
}

static void
parse_render_state(uint32_t *rsw_addr)
{
   int array_idx;
   uint32_t offset;
   uint32_t tex_addr = 0;
   array_idx = find_mem_block(*rsw_addr, &offset, NULL);

   if (array_idx) {
      int textures = 0;
      uint32_t i;
      uint32_t start = offset / 4;
      wrap_log("/* ============ RSW BEGIN ========================= */\n");
      for (i = start; i < (start + 16); i++) {
         wrap_log("/* 0x%08x (0x%08x) */",
                  (i * 4) + mali_addresses[array_idx - 1].physical,
                  (i - start) * 4);
         textures = parse_rsw(mali_addresses[array_idx - 1].address,
                              mali_addresses[array_idx - 1].physical, start, i - start);
         if (textures)
            tex_addr = textures;
      }
      wrap_log("/* ============ RSW END =========================== */\n");
      wrap_log("\n");
   }

   if (tex_addr)
      find_textures(tex_addr);
}

uint32_t
parse_plbu(uint32_t desc[],
           int stop,
           int start,
           int plbu_end,
           unsigned physical,
           int continued)
{
   uint32_t *value1;
   uint32_t *value2;
   uint32_t ret = 0;
   int end;
   int i;

   if (plbu_end < start)
      end = stop;
   else
      end = MIN(plbu_end, stop);

   if (!continued)
      wrap_log("/* ============ PLBU CMD STREAM BEGIN ============= */\n");
   else
      wrap_log("/* ============ PLBU CMD STREAM CONTINUED ========= */\n");

   for (i = start; i < end; i += 2) {
      value1 = &desc[i];
      value2 = &desc[i + 1];
      wrap_log("/* 0x%08x (0x%08x) */\t0x%08x 0x%08x",
               (i * 4) + physical, (i - start) * 4, *value1, *value2);

      if ((*value2 & 0xffe00000) == 0x00000000)
         parse_plbu_draw_arrays(value1, value2);
      else if ((*value2 & 0xffe00000) == 0x00200000)
         parse_plbu_draw_elements(value1, value2);
      else if ((*value2 & 0xff000fff) == 0x10000100)
         parse_plbu_indexed_dest(value1, value2);
      else if ((*value2 & 0xff000fff) == 0x10000101)
         parse_plbu_indices(value1, value2);
      else if ((*value2 & 0xff000fff) == 0x10000102)
         parse_plbu_indexed_pt_size(value1, value2);
      else if ((*value2 & 0xff000fff) == 0x10000105)
         parse_plbu_viewport_bottom((float *)value1, value2);
      else if ((*value2 & 0xff000fff) == 0x10000106)
         parse_plbu_viewport_top((float *)value1, value2);
      else if ((*value2 & 0xff000fff) == 0x10000107)
         parse_plbu_viewport_left((float *)value1, value2);
      else if ((*value2 & 0xff000fff) == 0x10000108)
         parse_plbu_viewport_right((float *)value1, value2);
      else if ((*value2 & 0xff000fff) == 0x10000109)
         parse_plbu_tiled_dimensions(value1, value2);
      else if ((*value2 & 0xff000fff) == 0x1000010a)
         parse_plbu_unknown_1(value1, value2);
      else if ((*value2 & 0xff000fff) == 0x1000010b) /* also unknown_2 */
         parse_plbu_primitive_setup(value1, value2);
      else if ((*value2 & 0xff000fff) == 0x1000010c)
         parse_plbu_block_step(value1, value2);
      else if ((*value2 & 0xff000fff) == 0x1000010d)
         parse_plbu_low_prim_size((float *)value1, value2);
      else if ((*value2 & 0xff000fff) == 0x1000010e)
         parse_plbu_depth_range_near((float *)value1, value2);
      else if ((*value2 & 0xff000fff) == 0x1000010f)
         parse_plbu_depth_range_far((float *)value1, value2);
      else if ((*value2 & 0xff000000) == 0x28000000)
         parse_plbu_array_address(value1, value2);
      else if ((*value2 & 0xf0000000) == 0x30000000)
         parse_plbu_block_stride(value1, value2);
      else if (*value2 == 0x50000000)
         parse_plbu_end(value1, value2);
      else if ((*value2  & 0xf0000000)== 0x60000000)
         parse_plbu_semaphore(value1, value2);
      else if ((*value2  & 0xf0000000)== 0x70000000)
         parse_plbu_scissors(value1, value2);
      else if ((*value2  & 0xf0000000)== 0x80000000) {
         if (parse_plbu_rsw_vertex_array(value1, value2))
            parse_render_state(value1);
      }
      else if ((*value2  & 0xf0000000)== 0xf0000000) {
         parse_plbu_continue(value1, value2);
         return *value1;
      }
      else
         wrap_log("\t/* --- unknown cmd --- */\n");
   }
   wrap_log("/* ============ PLBU CMD STREAM END =============== */\n");
   wrap_log("\n");

   return ret;
}
